from django.db import models
from django.utils import timezone
import colorsys

# Create your models here.


class Entry(models.Model):
    date = models.DateField()


    def days(self):
        delta = self.date - timezone.now().date()
        return delta.days

    def color(self):
        saturation = min(1, (self.days() / 70))
        saturation = max(saturation, 0)
        saturation = 1 - saturation
        r,g,b = colorsys.hsv_to_rgb(1, saturation, 1)
        col = '#%02x%02x%02x' % (int(r*255), int(g*255), int(b*255))
        return col


    def __str__(self):
        return str(self.date)
