function addSuggestions() {
  console.log('einrichtung')
  $('#id_name').on('input', handleSuggestions);
}

function handleSuggestions() {
  inhalt = $('#id_name').val()
  if (inhalt.length<2) {
    return;
  }
  $.ajax("http://localhost:8001/lernen/q/" + inhalt, {
    'success': function(data){
      console.log('neue daten: ' + JSON.stringify(data))
      html = '<ul>'
      if (data.entries.length==1){
        $("#id_text").val(data.entries[0].text);
      }
      for (entry of data.entries){
          html += '<li>' + entry['name'] + '</li>'
      }
      html += '</ul>'
      $("#suggestions").html(html)
    

    }
  })
}

$(document).ready(addSuggestions)
