from django.apps import AppConfig


class FarbenConfig(AppConfig):
    name = 'farben'
