from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.

from . import models

def main(request):
    context = {}
    if request.POST:
        form = models.EntryForm(request.POST)
        if form.is_valid():
            form.save()
            form = models.EntryForm()
    else:
        form = models.EntryForm()
    context['form'] = form 
    return render(request, 'lernen/main.html', context)


def suggestions(request, query):
    entries = models.Entry.objects.filter(name__contains=query)
    result = {
                'entries': []
            }
    for entry in entries:
        result['entries'].append({ 'name': entry.name, 'text': entry.text, 'id': entry.id })
    return JsonResponse(result)
    
