from django.db import models
from django.forms import ModelForm

# Create your models here.


class Entry(models.Model):
    name = models.CharField(max_length=200)
    text = models.TextField(blank=True)


class EntryForm(ModelForm):
    class Meta:
        model = Entry
        fields = ['name', 'text']
