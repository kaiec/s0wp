# Schiffe versenken

https://hackmd.io/99vZ8s_XTSm2ophBERSPzA?edit

## Regeln
https://de.wikipedia.org/wiki/Schiffe_versenken

- 10x10 Feld, Feld konfigurierbar? 
- Schiffe: 1x5, 2x4, 3x3, 4x2 (random?)
- Dürfen auch am Eck nicht aneinanderstoßen, außenrum entweder Rand oder Leer



## Model


## Views
- anklickbare Felder
- Spielfeld
- Button: Spielfeld prüfen
- Button: Löschen



## Todos
- 21.4.:
    - field Hilfsfunktionen in Model oder eigenes Modul
    - check Funktion eventuell auslagern?
    - Player in model analog zu Field aus Game auslagern
    - Das eigentliche Spiel fertig machen.
    - Design, CSS
    - Konzept zur Entwicklung, Dokumentation