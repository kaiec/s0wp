from django.shortcuts import render

# Create your views here.

from . import models

def list(request):
    context = {}
    context['entries'] = models.Entry.objects.all().order_by('date')
    return render(request, 'farben/list.html', context)


