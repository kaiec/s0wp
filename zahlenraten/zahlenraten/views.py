from django.shortcuts import render
from django.http import HttpResponse
import random

# Create your views here.

import random

zahl = None
versuche = []
feedback = ""

def game(request):
    global zahl
    global versuche
    global feedback
    context = {}
    if zahl == None:
        zahl = random.randint(1,100)
        versuche = []
    if request.POST:
        try:
            geraten = int(request.POST['geraten'])
            if geraten == zahl:
                feedback = 'gewonnen. hab mir schon ne neue zahl gedacht, mach doch weiter.'
                zahl = None
            elif geraten > zahl:
                feedback = 'zu groß'
            else:
                feedback = 'zu klein'
            versuche.append(geraten)
        except:
            context['error'] = 'gib eine zahl ein!'

    context['versuche'] = versuche
    context['feedback'] = feedback

    return render(request, "zahlenraten/index.html", context)
