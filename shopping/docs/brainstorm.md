## Einkaufsliste

URL: [HackMD](https://hackmd.io/61_eBR2tTNaQxOK5DCtxHw)

### Views:

* **Index:** 
    * Anzeige der Einkaufsliste mit unerledigten Dingen
    * Formularfelder zum Hinzufügen
    * Knopf zum Löschen
    * Knopf zum Abhaken
    * Zweite Liste mit erledigten Dingen
    * Design-Prinzip: Nur eine View

![Index View](index.png)

### Models:
* **Item:**
    * Text, Typ text
    * Menge, Typ text
    * Erledigt, Typ boolean
