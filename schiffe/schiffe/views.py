from django.shortcuts import render, get_object_or_404, redirect, reverse

from . import models

# Create your views here.
def choose_player(request):
    if "add" in request.POST:
        print(request.POST)
        player = models.Player()
        player.name = request.POST['name']
        if 'computer' in request.POST:
            player.computer = True
        player.save()
    players = models.Player.objects.all()
    context = {} 
    context['players'] = players
    return render(request, 'schiffe/choose_player.html', context)


def get_field(field, row, col, num_rows, num_cols):
    number_of_rows = num_rows
    number_of_columns = num_cols
    if col >= number_of_columns or col < 0 or row >= number_of_rows or row < 0:
        return '0'
    return field[row * number_of_columns + col]



def set_field(field, row, col, value, num_rows, num_cols):
    number_of_rows = num_rows
    number_of_columns = num_cols
    if col >= number_of_columns or col < 0 or row >= number_of_rows or row < 0:
        return field
    pos = row * number_of_columns + col
    field = field[:pos] + value + field[pos + 1:]
    return field

def field2lists(field, rows, cols):
    result = []
    for row in range(rows):
        row_list = []
        for col in range(cols):
            row_list.append(get_field(field, row, col, rows, cols))
        result.append(row_list)
    return result



def add_ship(game, name, length, count):
    ship = models.Ship()
    ship.game = game
    ship.name = name
    ship.length = length
    ship.count = count
    ship.save()

def check_field(field, ships, num_rows, num_cols):
    rows = num_rows
    cols = num_cols
    for row in range(rows):
        for col in range(cols):
            if get_field(field, row, col, num_rows, num_cols) == 'x':
                count = 1
                dir = [1, 0] # down
                if get_field(field, row, col + 1, num_rows, num_cols) == 'x':
                    dir = [0, 1] # right
                r = row
                c = col
                while True:
                    r = r + dir[0]
                    c = c + dir[1]
                    if get_field(field, r, c, num_rows, num_cols) == 'x':
                        count += 1
                    else:
                        end_row = r - dir[0]
                        end_col = c - dir[1]
                        # Schiff ist "fertig"
                        # row/col ist startpostion
                        # end_row/end_col ist endposition
                        if count in ships:
                            ships.remove(count)
                        else:
                            return("Ungültiges Feld, Schiff mit Länge {} an Position {}/{} ist zu viel.".format(count, row + 1, col + 1))
                        #  .... über dem schiff muss alles frei sein
                        #  .xx. beim schiff links und rechts
                        #  .... und drunter wieder alles
                        # Abstand zu anderen Schiffen prüfen, startpos - (1,1)
                        for c in range(col - 1, end_col + 1 + 1):
                            if get_field(field, row - 1, c, num_rows, num_cols) == 'x':
                                return("Kein Abstand über Schiff mit Länge {} and Position {}/{}".format(count, row + 1, col + 1))
                            if get_field(field, end_row + 1, c, num_rows, num_cols) == 'x':
                                return("Kein Abstand unter Schiff mit Länge {} and Position {}/{}".format(count, row + 1, col + 1))
                        for r in range(row, end_row + 1):
                            if get_field(field, r, col-1, num_rows, num_cols) == 'x':
                                return("Kein Abstand links neben Schiff mit Länge {} and Position {}/{}".format(count, row + 1, col + 1))
                            if get_field(field, r, end_col+1, num_rows, num_cols) == 'x':
                                return("Kein Abstand rechts neben Schiff mit Länge {} and Position {}/{}".format(count, row + 1, col + 1))
                        # Felder markieren
                        for r in range(row - 1, end_row + 1 + 1):
                            for c in range(col - 1, end_col + 1 + 1):
                                field = set_field(field, r, c, 'd', num_rows, num_cols)
                        break
    if len(ships)>0:
        return("Ships not found: {}".format(ships))
    print(field)
    return None

def setup_game(request, player_id, game_id):
    player1 = get_object_or_404(models.Player, id=player_id)
    if game_id == 0:
        game = models.Game()
        game.player1 = player1
        game.num_cols = 10
        game.num_rows = 10
        game.save()
        field1 = models.Field()
        field1.game = game
        field1.player = player1
        field1.field = 100 * '.'
        field1.save()
        add_ship(game, "Schlachtschiff", 5, 1)        
        add_ship(game, "Kreuzer", 4, 2)        
        add_ship(game, "Zerstörer", 3, 3)        
        add_ship(game, "U-Boot", 2, 4)        
        game.refresh_from_db()
        return redirect(reverse('setup_game', args=[player1.id, game.id]))
    else:
        game = get_object_or_404(models.Game, id=game_id)
        field1 = game.field_set.get(player=player1)

    # Ab hier haben wir so oder so ein Spiel mit Feld

    # Klick auf ein Feld zum Setzen eines Schiffs
    if 'row' in request.GET and 'col' in request.GET:
        row = int(request.GET['row'])
        col = int(request.GET['col'])
        old = get_field(field1.field, row, col, game.num_rows, game.num_cols)
        if old=='.':
            new = 'x'
        else:
            new = '.'
        field1.field = set_field(field1.field, row, col, new, game.num_rows, game.num_cols)
        field1.save()
        return redirect(reverse('setup_game', args=[player1.id, game.id])) 
    

    # Vorbereiten der Antwortseite
    context = {}
    context['player1_id'] = player1.id
    context['game_id'] = game.id
    context['field_list'] = field2lists(game.field_set.get(player=player1).field, game.num_rows, game.num_cols)
    context['ships'] = game.ship_set.all()

    # Ggf. prüfen und Ergebnis mit ausgeben
    if 'check' in request.GET:
        ships=[]
        for ship in game.ship_set.all():
            for i in range(ship.count):
                ships.append(ship.length)
        msg = check_field(field1.field, ships, game.num_rows, game.num_cols)
        if msg == None:
            msg = "Alles ok!"
        context['message'] = msg

    return render(request, 'schiffe/setup_game.html', context)