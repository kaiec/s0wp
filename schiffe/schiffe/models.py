from django.db import models
from django.utils import timezone

# Create your models here.

class Player(models.Model):
    name = models.TextField(null=False, blank=False)
    computer = models.BooleanField(default=False, null=False)

    def __str__(self):
        comp = " (Computer)"
        if not self.computer:
            comp = ""
        return "{}{}".format(self.name, comp)


class Game(models.Model):
    player1 = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='game_p1')
    player2 = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='game_p2', null=True)
    num_rows = models.IntegerField(default=10)
    num_cols = models.IntegerField(default=10)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{} - {} ({})".format(self.player1, self.player2, self.timestamp) 
    

class Field(models.Model):
    field = models.TextField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)


class Ship(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    name = models.TextField(null=False, blank=False)
    length = models.IntegerField()
    count = models.IntegerField()

    def __str__(self):
        return "{} ({}, {}x)".format(self.name, self.length, self.count)
    