from django.apps import AppConfig


class ZahlenratenConfig(AppConfig):
    name = 'zahlenraten'
